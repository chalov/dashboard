(function (angular) {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }

    var uuid = function () {
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
            s4() + '-' + s4() + s4() + s4();
    };


    angular.module('dashboard', [])

        //Storage Factory
        .factory('storage', [function () {
            return {
                get: function (key) {
                    var item = localStorage.getItem(key);
                    try {
                        return JSON.parse(item);
                    } catch (e) {

                    }
                    return item;
                },
                set: function (key, value) {
                    try {
                        value = JSON.stringify(value);
                    } catch (e) {

                    }
                    localStorage.setItem(key, value);
                }
            }
        }])

        /*
         Удаляет отображение милисекунд в <input type="time">,
         https://forum.ionicframework.com/t/how-can-i-remove-milliseconds-from-a-time-input-field-in-android/30469
         https://mark.zealey.org/2015/01/08/formatting-time-inputs-nicely-with-angularjs
         */
        .directive('ngModel', ['$filter', function ($filter) {
            return {
                require: '?ngModel',
                link: function (scope, elem, attr, ngModel) {
                    if (!ngModel) return;
                    if (attr.type !== 'time') return;

                    ngModel.$formatters.unshift(function (value) {
                        return value.replace(/:\d{2}[.,]\d{3}$/, '')
                    });
                }
            }
        }])

        //MainController
        .controller('main', ['storage', '$scope', '$interval', '$timeout', function (storage, $scope, $interval, $timeout) {

            var form = function () {
                    var now = new Date();
                    return {
                        title: '',
                        description: '',
                        date: now,
                        time: new Date(now.setMinutes(now.getMinutes() + 10))
                    }
                },
                list = function () {
                    var list = storage.get('dashboard');
                    !Array.isArray(list) && (list = []);
                    return list;
                };

            $scope.form = form();
            $scope.dashboard = list();
            $scope.down_times = {};
            calculateDownTime();
            sort();

            $scope.actions = {
                add: function () {
                    $scope.add_form.date_err = false;
                    var now = Date.now(),
                        form_date = $scope.form.date,
                        form_time = $scope.form.time,
                        end = new Date(
                            form_date.getFullYear(),
                            form_date.getMonth(),
                            form_date.getDate(),
                            form_time.getHours(),
                            form_time.getMinutes()
                        );

                    if (now > end)
                        return ($scope.add_form.date_err = true) && $timeout(function () {
                                $scope.add_form.date_err = false;
                            }, 2000);
                    $scope.dashboard.push({
                        title: $scope.form.title,
                        description: $scope.form.description,
                        id: uuid(),
                        expires: end
                    });
                    save();
                    $scope.form = form();
                },
                remove: function (id) {
                    for (var i = 0, ii = $scope.dashboard.length; i < ii; i++) if ($scope.dashboard[i].id === id) {
                        $scope.dashboard.splice(i, 1);
                        return save();
                    }
                },
                add_time: function (id) {
                    for (var i = 0, ii = $scope.dashboard.length; i < ii; i++) if ($scope.dashboard[i].id === id) {
                        var expires = new Date($scope.dashboard[i].expires);
                        expires < Date.now() && (expires = new Date());
                        $scope.dashboard[i].expires = new Date(expires.setMinutes(expires.getMinutes() + 10));
                        return save();
                    }
                }
            };

            function sort() {
                $scope.dashboard.sort(function (a, b) {
                    return new Date(a.expires) - new Date(b.expires);
                });
            }

            function save() {
                sort();
                storage.set('dashboard', $scope.dashboard);
                calculateDownTime();
            }

            function calculateDownTime() {
                for (var i = 0, ii = $scope.dashboard.length; i < ii; i++) {
                    var id = $scope.dashboard[i].id,
                        expires_date = new Date($scope.dashboard[i].expires).getTime(),
                        now = new Date().getTime();
                    if (now > expires_date + (1000 * 60 * 2)) {
                        $scope.dashboard.splice(i, 1);
                        continue;
                    }
                    var diff = expires_date - now,
                        ms_diff = diff / 1000,
                        seconds = Math.floor(ms_diff % 60);
                    ms_diff /= 60;
                    var minutes = Math.floor(ms_diff % 60);
                    ms_diff /= 60;
                    var hours = Math.floor(ms_diff);
                    minutes < 10 && (minutes = '0' + minutes);
                    hours < 10 && (hours = '0' + hours);
                    seconds < 10 && (seconds = '0' + seconds);
                    var expiration = diff <= 0;
                    $scope.down_times[id] = {
                        remainder: expiration ? '00:00:00' : hours + ':' + minutes + ':' + seconds,
                        expiration: expiration
                    };
                }
            }

            $interval(calculateDownTime, 1000);
        }]);
})(window.angular);